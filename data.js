'use strict';

class Link {
  constructor(name, icon, url) {
    this.name = name;
    this.icon = icon;
    this.url = url;
  }
}

let data = {
  name: 'Doof Spoofer',
  links: [
    new Link('Github', 'fa fa-github-alt', 'https://github.com/doof'),
    new Link('Chugs', 'fa fa-beer', 'http://fosters-developer-edition.na30.force.com/'),
    new Link('Burritos', 'fa fa-cutlery', 'https://burrengus-portal.herokuapp.com/')
  ]
}

module.exports = data;
